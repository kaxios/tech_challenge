
import org.apache.spark.sql.{Column, DataFrame}
import org.apache.spark.sql.functions.{lit, udf}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{StructType, StructField, StringType,LongType}
//import org.apache.spark.sql.types._
import org.apache.spark.sql.SparkSession
//import org.apache.spark.sql.types._
import org.apache.spark.SparkConf
import org.apache.spark._
import org.apache.spark.sql._
import org.apache.spark.sql.expressions.UserDefinedFunction
//import scala.collection.mutable.WrappedArray
import scala.collection.mutable.{ArrayBuffer,WrappedArray}


val spark = SparkSession.builder
                .appName("puzzle")
                .getOrCreate()


// your handle to SparkContext to access other context like SQLContext
val sc = spark.sparkContext
val sqlContext = new org.apache.spark.sql.SQLContext(sc)

// 1) Read Dict into Dataframe
val pathFreqDict = "/home/jovyan/spark_exercise/transposed.json"
val freqDict = spark.read.option("multiLine", true).option("mode", "PERMISSIVE").json(pathFreqDict)

// 2) Read Puzzle into Dataframe
val pathPuzzle = "/home/jovyan/spark_exercise/puzzles.json"
val puzzle = spark.read.option("multiLine", true).option("mode", "PERMISSIVE").json(pathPuzzle)


val allLetters = Seq("a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","v","w","x","y","z")

val allLettersString = "abcdefghijklmnopqrstvwxyz".toArray

// 3 ) Process initial files to help its use
//change puzzleId for a different puzzle
val allPuzzles = puzzle.select(explode($"puzzles").as("puzzles"))
                        .select("puzzles.puzzle")
                        .withColumn("group",lit("file"))
                        .groupBy("group")
                        .agg(collect_list(col("puzzle")) as "puzzles")
                        .select("puzzles")
                        .first().getAs[WrappedArray[String]](0).toArray


// 4 ) Iterate on all puzzles on the file
for( currentPuzzleFile <- allPuzzles) {
     println("solving puzzle:" + currentPuzzleFile)
    val puzzleName = "'"+ currentPuzzleFile +"'"

    def sortStringFunc: (String => String) = { s => new String(s.toLowerCase.toCharArray.sorted) }
    val sortStringUDF = udf(sortStringFunc)

    //Sort freqDict into freqDictSorted
    val puzzleFilter = "puzzle =" + puzzleName
    val freqDictSorted = freqDict
                            .filter("key is not null" )
                            .withColumn("keySorted",sortStringUDF(col("key").cast("string")) )
                            .withColumn("keySize",length(col("key")  ) )
                            .withColumn("keyLetterArray",split($"key", "") )

    val maxDictFreq = freqDictSorted.select(max($"value")).as[Long].first + 1000
//    print(maxDictFreq)

    val puzzleMin = puzzle.select(explode($"puzzles").as("puzzles"))
                             .select("puzzles.*")
                             .withColumn("scramblesExploded", explode($"scrambles"))
                             .select("finalScrambleLen","puzzle", "scramblesExploded.letters","scramblesExploded.circles" )
                             .withColumn("sortedScramble",sortStringUDF(col("letters")) )
                             .withColumn("uniqueID",concat(lit("cid_"),monotonically_increasing_id))
                             .filter(puzzleFilter)

// 5 ) Find possible match words
    val matches = puzzleMin.join(freqDictSorted, $"sortedScramble" === $"keySorted")
                    .sort("puzzle","uniqueID")

//    puzzleMin.show()
//    matches.show()

    val totalAnagramsPhase1 = puzzleMin.count
    println(totalAnagramsPhase1)

    val lettersByScrambleCandidate = matches.select($"uniqueID",$"letters", $"puzzle",$"key", $"value", explode($"circles").as("candidateIdx"))
                                        .withColumn("candidateCharList",split($"key",""))
                                        .withColumn("candidateChar",expr("candidateCharList[candidateIdx]") )
                                        .groupBy("puzzle", "letters", "uniqueID", "key" )
                                        .agg(
                                            collect_list(col("candidateChar")) as "candidateChars"
                                        )
                                        .withColumn("candidateString", concat_ws("", $"candidateChars" ) )
                                        .withColumn("candidatePair", array($"candidateString",$"key") )

// 6 ) Pivot possible matches of anagrams to be easier to work
    val rotatedPossibilities = lettersByScrambleCandidate.groupBy("puzzle")
            .pivot("uniqueID")
            .agg(collect_set("candidatePair").as("candidatePair"))


//    rotatedPossibilities.show
    //println(uniqueIDList)

    //totalAnagramsPhase1

    val anagramsColumns = puzzleMin.groupBy("puzzle")
                    .agg(collect_list(col("uniqueID")) as "uniqueIDList")
                    .select("uniqueIDList")
                    .first().getAs[WrappedArray[String]](0).toArray

// 7 ) Explode Possibilities  of composing final result for exploring final result scenarios.

    var anagramColumnName = new Array[String](totalAnagramsPhase1.toInt)
    var anagramColumnExplodedName = new Array[String](totalAnagramsPhase1.toInt)
    var anagramColumnGetItemZero = new Array[String](totalAnagramsPhase1.toInt)
    var anagramColumnGetItemOne = new Array[String](totalAnagramsPhase1.toInt)
    var currentAnagram = 0
    for ( a <- anagramsColumns ) {
         var columnName =  a
         var columnExplodedName =  a + "_exploded"
         anagramColumnName(currentAnagram) = columnName
         anagramColumnExplodedName(currentAnagram) = columnExplodedName
         anagramColumnGetItemZero(currentAnagram) = columnExplodedName + "[0]"
         anagramColumnGetItemOne(currentAnagram) = columnExplodedName + "[1]"
         currentAnagram = currentAnagram + 1
         println( "Value of a: " + a );
    }
    println(anagramColumnExplodedName.mkString(","))
    println(anagramColumnName.mkString(","))

    var automateExplodePossibilities = rotatedPossibilities
    currentAnagram = 0
    for ( a <- anagramsColumns ) {
        val exprExplode = "explode( "+anagramColumnName(currentAnagram) + " )"
        println("Exploding:" + exprExplode)
        println("Exploding:" + anagramColumnName(currentAnagram)  + "--" + anagramColumnExplodedName(currentAnagram) )

        automateExplodePossibilities = automateExplodePossibilities.withColumn(anagramColumnExplodedName(currentAnagram),expr(exprExplode) )
                        .drop(anagramColumnName(currentAnagram))
        currentAnagram = currentAnagram + 1
    }

    automateExplodePossibilities =   automateExplodePossibilities.withColumn("possibilityId",concat(lit("combiId_"),monotonically_increasing_id))

//    automateExplodePossibilities.show()

// 8 ) Clean Explosion intermediaries
    var cleanPossibilitiesConcat = "concat( " +  anagramColumnGetItemZero.mkString(",") + ")"
    var cleanPossibilities = automateExplodePossibilities.withColumn("word", expr( cleanPossibilitiesConcat ) )
                                                                     .select("puzzle", "possibilityId","word")
//    cleanPossibilities.show()

// 9 ) Reduce size of used dictionary by removing impossible letters from search
    val allPossibleLetter = cleanPossibilities.groupBy("puzzle").agg(collect_list(col("word"))   as "candidateLetters").select(concat_ws("", $"candidateLetters") ).first().getString(0)

    val allPossibleLetterDistinct = allPossibleLetter.toArray.distinct
    val notPossibleLetter = allLettersString.filterNot(allPossibleLetterDistinct.contains(_)).mkString("|")
    //allLetters.diff(allPossibleLetterDistinct)

    //Letters that are not available to form a word
    println(">>" + notPossibleLetter)


    //TODO: optimize this query
    val possibleDict = freqDictSorted
                        .withColumn("notPossibleLetters",lit(notPossibleLetter))
                        .withColumn("containNotPossibleLetters",col("key").rlike(notPossibleLetter))
                        .drop("notPossibleLetters")
                        .filter("containNotPossibleLetters == false")

//    println("pre:"+ freqDictSorted.count())
//    println("post:"+ possibleDict.count())

//    possibleDict.show

//    freqDictSorted.printSchema

    //val diffStringContent = udf { ( a:  String,  b:  String ) => a.toArray diff b }
    val wordSizes = matches.filter(puzzleFilter)
                        .select("finalScrambleLen")
                        .first().getAs[WrappedArray[Long]](0).toArray
//    println(wordSizes)

    //Loop words

    //iterate by      cleanPossibilities.word

// 10) Explore word candidates
    val wordCandidate = cleanPossibilities.groupBy("puzzle")
                    .agg(collect_list(col("word")) as "wordCandidate")
                    .select("wordCandidate")
                    .first().getAs[WrappedArray[String]](0).toArray

    val schemaCandidate = StructType(
      StructField("word", StringType, true) ::
      StructField("part", StringType, true) ::
      StructField("partSorted", StringType, true) :: Nil)
    var initialDF = spark.createDataFrame(sc.emptyRDD[Row], schemaCandidate)


    var finalWordPos = 0
    var wordPossibilities =  Seq("not_valid").toDF("word")
    //var localPossibilities =  Seq("not_valid").toDF("word")

    var globalPossibilities =  Seq("not_valid").toDF("word")

    var columnsScore = new Array[String](wordSizes.length)
    var columnsName = new Array[String](wordSizes.length)
    var finalColumnsPrefix = "final_"
    for (n <- wordSizes) {
         var columnWordName =  finalColumnsPrefix + "word_" + finalWordPos
         var columnWordScore =  finalColumnsPrefix +  "word_score_" + finalWordPos
         globalPossibilities = globalPossibilities.withColumn(columnWordName, lit("null") )
                                                    .withColumn(columnWordScore, lit(0) )
         columnsScore(finalWordPos) = columnWordScore
         columnsName(finalWordPos) = columnWordName
         finalWordPos = finalWordPos + 1
    }
    val sumScore = columnsScore.mkString(" + ")
    val productScore = columnsScore.mkString(" * ")
    val concatCheck = columnsName.mkString(" , ")
    println(sumScore)
    globalPossibilities = globalPossibilities
                                .withColumn("solutionScoreSum", expr(sumScore )  )
                                .withColumn("solutionScoreProduct", expr(productScore )  )
                                .withColumn("concatCheck", lit(" ")  )

//    wordPossibilities.show

// 11) Explore distinct combinations of final word configuration (per word)
    for (m <- wordCandidate) {
         var currentWord = m
         println("processing current word: " + m)
    //     println("processing current word: " + wordSizes.map(_.toString))
    //     wordPossibilities.show
        var localPossibilities =  Seq(m).toDF("word")
         var finalWordPos = 0
         for (n <- wordSizes) {
             var columnWordName =  finalColumnsPrefix +  "word_" + finalWordPos
             var columnWordScore =  finalColumnsPrefix +  "word_score_" + finalWordPos


                 println("processing wordSizes: " + n )
                 val possibilities = currentWord.toArray.combinations(n.toInt).toList.map(new String(_))
                 val possibilityDF = possibilities.toDF("part")
                                        .withColumn("partSorted",sortStringUDF(col("part").cast("string")))
                                        .withColumn("wordCandidate",lit(m))
                                        .join(possibleDict,  $"partSorted" === $"keySorted")
                                        .withColumn("correctedValue",when($"value"  ===  lit(0), lit( maxDictFreq )).otherwise($"value") )
                                        .select($"wordCandidate",$"key".alias(columnWordName),$"correctedValue".alias(columnWordScore) )
                 // possibilityDF.show
                 localPossibilities = localPossibilities.join(possibilityDF, $"word" === $"wordCandidate" ).drop("wordCandidate")


             finalWordPos = finalWordPos + 1
         }

    //     localPossibilities.show
        var concatExpr = "concat( " +  concatCheck + " )"
        println("concat"+ concatExpr)
// 12) Append only "good solutions" to final possibilities dataframe
         localPossibilities = localPossibilities
                                .withColumn("solutionScoreSum", expr(sumScore )  )
                                .withColumn("solutionScoreProduct", expr(productScore )  )
                                .withColumn("concatCheck", expr(concatExpr ) )
                                .withColumn("concatCheckSorted", sortStringUDF(col("concatCheck").cast("string")) )
                                .withColumn("wordSorted", sortStringUDF(col("word").cast("string")) )
                                .filter("wordSorted = concatCheckSorted" )
                                .drop("concatCheckSorted","wordSorted")

         globalPossibilities = globalPossibilities.union(localPossibilities)
//         println("processing localPossibilities df: " + localPossibilities.count )
    }


    val possibleSolutions = globalPossibilities
            .filter("word != 'not_valid'")
            .sort("solutionScoreSum")
            .limit(1)

//    println("processing possibleSolutions df: " + possibleSolutions.count )

    // Final Result
     possibleSolutions.show(200,false)
    // Cleaning Result

// 13)  Clean Final result
    var finalSolution = possibleSolutions
                            .withColumn("pword",$"word" ).drop("word")
                            .join(cleanPossibilities,  $"word" === $"pword")
                            .drop("pword","concatCheck")
                            .withColumn("ppuzzle",$"puzzle" ).drop("puzzle")
                            .withColumn("pword",$"word" ).drop("word")
                            .withColumn("ppossibilityId",$"possibilityId" ).drop("possibilityId")
                            .join(automateExplodePossibilities,  $"possibilityId" === $"ppossibilityId")
                            .drop("ppuzzle","ppossibilityId", "pword" , "possibilityId")

    currentAnagram = 0
    for ( a <- anagramsColumns ) {
        println(a)
        val exprClean = anagramColumnGetItemOne(currentAnagram)
        val columnCleanName = "anagram_" + currentAnagram
        println(exprClean)
        finalSolution = finalSolution.withColumn(columnCleanName,expr(exprClean)  )
                                     .drop(anagramColumnExplodedName(currentAnagram))
        currentAnagram = currentAnagram + 1

    }
    finalSolution.printSchema
    finalSolution.show
// 14)  Save Final Result
    finalSolution
       .coalesce(1)
       .write
        .mode(SaveMode.Overwrite)
        .format("json")
       .save("/home/jovyan/spark_exercise/finalSolution_"+ currentPuzzleFile+".json")
     println("solved puzzle" + currentPuzzleFile + "!!!")
}


