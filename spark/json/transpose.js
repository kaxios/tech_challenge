var fs = require('fs');

var input = JSON.parse(fs.readFileSync('/root/files/freq_dict.json'));

var out = Object.keys(input).map(function(data){
    //console.log(data + "--" + input[data])
    return { key: data, value: input[data] } ;
});

fs.writeFileSync('/root/files/transposed.json', JSON.stringify(out));