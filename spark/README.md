# Problem

The jumble puzzle is a common newspaper puzzle, it contains a series of anagrams that must be solved (see https://en.wikipedia.org/wiki/Jumble). To solve, one must solve each of the individual jumbles. The circled letters are then used to create an additional anagram to be solved.

In especially difficult versions, some of the anagrams in the first set can possess multiple solutions. To get the final answer, it is important to know all possible anagrams of a given series of letters.

Your challenge is to solve the five Jumble puzzles using Spark with Scala or Python, where it makes sense to do so. If the final puzzle has multiple possible answers, you are to include an algorithm to determine the most likely one. We have provided a dictionary where the "most common" English words are scored (1=most frequent, 9887=least frequent, 0=not scored due to infrequency of use). For each puzzle, produce the "most likely" (as you determine it) final anagram produced from solving all the other anagrams.

Important Notes: Part of your task is to have this be as production ready as possible - while there are only five puzzles now, assume that there could be many more, so use Spark in the most useful way, however you don't need to spend a lot of time on tweaking the parallelization parameters. The code should be deployable and maintainable as well. Don't spend more than 24 hours to complete as much of the assignment as you can.

Also included:

- freq_dict - Keys are English Dictionary words to be used in your solving of the jumbles.
Non-zero values are the frequency rankings (1=most frequent). Zero values mean that the word is too infrequent to be ranked.
- Pictures of the jumbles we provided for you to solve - You can put these in whatever data format you'd like for your program to read in.
Please include the following in your repository:
- Your initial data (from the jumble pictures given)
- Output from your code



![Images](./images/puzzle1.jpg)
![Images](./images/puzzle2.jpg)
![Images](./images/puzzle3.jpg)
![Images](./images/puzzle4.jpg)
![Images](./images/puzzle5.jpg)

# Code Pre-Solution

Server Setup/DataPreparation

```bash
cd /Users/kaxios/_DEV/_private/spark_studies/tech_challenges
docker run  -v $PWD/json:/root/files  node node /root/files/transpose.js

# upload transposed.json to spark cluster to easy consumption of dictionary of frequency.

# Remote cluster on digital ocean:
eval $(docker-machine env remote-spark-payed)
docker-compose -f ./spark/docker-compose.yaml up -d

open http://165.22.33.6:8888/?token=f6cd2e8f89d0283807945032ccd2bfd533e2a27237fc9866
```

# Code Solution
Scala Code
[Final solution - Scala Jupyter](./FinalSolutionScalaNotebook.scala)

# Match Score in details

- Calculated the Max frequency from dic + 1000 assigned to values with "0"
    > See lines: 66 and 243
- Created a score based on the sum of frequency/corrected value
    > See line: 271
- Sort by `solutionScoreSum` score and limited 1
    > the key was to move `zero` values to least preferred than lower numbers like 1.

# Solution Results:

[Files per puzzle](./result)

# Solution Plan


1) Read Dict into Dataframe

    > for this I used nodeJS script to do a quick transpose
    > [nodeJS](./json/transpose.js)
    > This was done to save time
    - Added a column with a sorted version of the `key`.

2) Read Puzzle into Dataframe

3) Process initial files to help its use

4) Iterate on all puzzles on the file

5) Find possible match words

6) Pivot possible matches of anagrams to be easier to work

7) Explode Possibilities  of composing final result for exploring final result scenarios.
    > Dynamic number of columns

8) Clean Explosion intermediaries


9) Reduce size of used dictionary by removing impossible letters from search

10) Explore word candidates

11) Explore distinct combinations of final word configuration (per word)

12) Append only `good solutions` to final possibilities dataframe
    > `good solutions` with possible mach from dictionary and also `anagram` fits available words as expected.

13)  Clean Final result

14)  Save Final Result


# Sample Puzzles File

```json
{
    "puzzles": [
        {
            "puzzle": "puzzle1.jpg",
            "scrambles": [
                { "letters":"NAGLD", "circles":[1]}
                # Circle starts on "0"

            ],
            "finalScrambleLen":[3,4,4]
        }
    ]
}
```




