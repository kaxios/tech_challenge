# Challenge

On your own computer, please create a single-page, Node.js application using a front-end framework (such as Angular 2+, React.js or Vue.js). The page should contain five text input fields on the left-hand side of the page, labeled as follows: First Name, Last Name, Address, Company and Salary. Additionally, make the fields required where appropriate.

Below the input fields, please place a ‘Submit’ button. When the user presses the button, the data in the five fields should be written to a database running on your machine. Also, when the user clicks the button, whatever text that appears in the fields on the left should be read from the database and displayed on the right side of the screen.

Note: If you choose to use a relational database, please make one table to store the data from the five fields. If you decide to use a document-based database, please use one document to store the five text boxes.

In a separate form, please include an interface that will calculate and display the costs of any of the companies persisted to the database based on the salary of its employees. These calculations should be as efficient as possible.

Extra points:

- Writing front and back-end tests
- Pushing the GitHub repository to a cloud hosting provider





# UI Pre-Plan

![New Data](./img/tech_challenge/tech_challenge.002.jpeg)

![List Data](./img/tech_challenge/tech_challenge.003.jpeg)

# Running Server

```bash

git clone https://cesarlbf@bitbucket.org/kaxios/tech_challenge.git

cd tech_challenge
docker build -t app_server_go fullstack/app-server
docker build -t app_ui fullstack/ui-server

# docker run -i -t -p 0.0.0.0:9090:8080 app_server_go:latest /root/main
docker-compose -f ./fullstack/docker-compose.yaml up


# To Send some test Data:
 ./fullstack/test.sh

 # Testing Live UI
 open http://165.227.120.8

```

# Solution

|  Microservices | Description |
|:---|:-----|
| ui-server | Angular Web app hosted by nginx |
| app-server | Go rest API generated based on a swagger definition than add some custom logic |
| database-server | MongoDB  |

Based on [Plan](./Plan.md)