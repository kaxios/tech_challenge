# Overview Solution

- [X] Define main interfaces (20 mins)
    > check: tech_challenge keynote
- [X] Define rest interface(swagger) (30 mins) - `fullstack/artifacts/swagger.yaml`
    > https://editor.swagger.io/?_ga=2.183870062.996756650.1561220038-387607330.1561220038
(break:  12:43)    
- [X] Gencode in go (30 mins) - `fullstack/app-server/src-server`
- [X] Create Curl to test interface(bash test) (20 mins) - `fullstack/test.sh`
- [X] Deploy mongo (30 mins)  - `fullstack/docker-compose.yaml`
- [X] Connect go code to mongoDB (30 mins)
    - [X] Setup mongo simple Connect to mongo
    - [X] Setup mongo write functions
    - [X] Setup mongo read 1 function
    - [ ] Setup mongo read list function - `Working on it: 18:25`
        - [X] Make the groupby work in mongocli
        - [X] Make groupby work on go
        - [X] Parse result
> Break: 18:30 
- [X] Add ui(angular.js) (2 hours)
- [X] GO dockerfile (30 mins)
- [X] Nginx/UI dockerfile (1 hour)
- [X] Docker-compose System connected (30 mins)
> Clock-out: 0:17

Extra: 
- [ ] Add go test(1h max) 
- [ ] Add Prometheus (30 mins)
- [ ] Add Grafana (1 hour)
- [ ] Add Envoy for extra metrics (30 mins)

Estimated: 10 hours

Extra tasks: 
- Review UI 
- Review Docs
- Review Deployment
- Try to deploy on some public clouds(digital ocean)