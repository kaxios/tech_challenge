package swagger

import (
	"log"
	"context"	
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	//"encoding/json"
)


func getMongoClient() (*mongo.Client, error) {
	opts := options.Client().
		SetHosts([]string{"database-server:27017"}).
		SetAuth(options.Credential{Username: "root", Password: "example"})

	log.Printf("Connecting to mongodb")
	client, err := mongo.NewClient(opts)
	if err != nil {
		log.Printf("Error connecting to mongo", err)
		return client, err
	}
	err = client.Connect(context.Background())
	if err != nil {
		log.Printf("Error in occured while establishing connection", err)
		return client, err
	}
	// Check the connection
	err = client.Ping(context.Background(), nil)
	if err != nil {
		log.Printf("Unable to ping connection", err)
		return client, err
	}
	log.Printf("Connected to MongoDB!")
	return client, nil

}


func createEmployeeMongo(employee Employee)(Employee, error) {
	clientMongo, err := getMongoClient()
	//employee.Id = bson.NewObjectId()
	defer clientMongo.Disconnect(context.Background())
	if err != nil {
		log.Printf("Unable to connect to mongo client", err)
		return Employee{}, err
	}
	collection := clientMongo.Database("mydb").Collection("employee")
	insertResult, err := collection.InsertOne(context.TODO(), employee)
	if err != nil {
		log.Printf("Unable to add to database",err)
		return Employee{}, err
	}
	employee.Id = insertResult.InsertedID.(primitive.ObjectID).Hex()
	log.Printf("Inserted a single document %s", insertResult.InsertedID.(primitive.ObjectID).Hex())
	return employee, nil
}


func getEmployeeMongo(id string)(Employee, error) {
	clientMongo, err := getMongoClient()
	defer clientMongo.Disconnect(context.Background())
	if err != nil {
		log.Printf("Unable to connect to mongo client", err)
		return Employee{}, err
	}
	collection := clientMongo.Database("mydb").Collection("employee")
	var employee Employee
	objID, _ := primitive.ObjectIDFromHex(id)
	filter :=  bson.M{"_id": objID}
	//bson.D{{"_id", id}}
	err = collection.FindOne(context.TODO(), filter).Decode(&employee)
	if err != nil {
		log.Printf("Error:", err)
	}
	
	log.Printf("Found a single document: %+v\n", employee)
	return employee, nil

}	
type GroupId struct {   
	Company string `json:"company,omitempty"`
}
type GroupElement struct {   
	Id GroupId `bson:"_id" json:"id,omitempty"`
	Value int64 `json:"value,omitempty"`
}


func getAllCompanies()([]Company, error) {
	clientMongo, err := getMongoClient()
	defer clientMongo.Disconnect(context.Background())
	if err != nil {
		log.Printf("Unable to connect to mongo client", err)
		return nil, err
	}
	collection := clientMongo.Database("mydb").Collection("employee")


	pipeline := bson.A{ bson.M{"$group":  bson.M{ "_id": bson.M{"company": "$company"} , "value": bson.M{"$sum": "$salary"} } } } 
	cursor, err1 := collection.Aggregate(context.Background(), pipeline, options.Aggregate())
	if err1 != nil {
		log.Printf("Error:", err1)
	}	
	
	var result []Company
	for cursor.Next(context.Background()) {
		elem := GroupElement{}
		log.Printf("Record: %s", cursor.Current)
		err = cursor.Decode(&elem)
		if err != nil {
			log.Printf("Error:", err)
		}
		log.Printf("Result: %s - %d", elem.Id.Company, elem.Value)
		log.Printf("Record:", elem)
		currentResut := Company{Company: elem.Id.Company, Value: elem.Value }
		result = append(result, currentResut)

		log.Printf("Result:", result)
	}

	return result, nil

}	
