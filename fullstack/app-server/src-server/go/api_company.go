/*
 * Swagger Company Value Calculator
 *
 * Sample Company Manager
 *
 * API version: 1.0.0
 * Contact: someone@gmail.com
 * Generated by: Swagger Codegen (https://github.com/swagger-api/swagger-codegen.git)
 */

package swagger

import (
	"net/http"
	"encoding/json"
	"log"	

)

func FindCompanyByNameList(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	company, err := getAllCompanies()
	if err != nil {
		log.Printf("Return Error", err)
	}
	js, err := json.Marshal(company)
	if err != nil {
		log.Printf("Parsing Error", err)
	}
	w.Write(js)	
	w.WriteHeader(http.StatusOK)
}
