Solutions for the proposed tech challenges

# 1 - Fullstack Dev

[Check Details at here](./fullstack/README.md)

# 2 - Spark Dev

[Check Details at here](./spark/README.md)